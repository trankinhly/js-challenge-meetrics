/************************************************************************************************
 *                                                                                              *
 *                              VARIABLES DECLARATION                                           *
 *                                                                                              *
 ************************************************************************************************/
var adIsViewable = true,
  viewabilityTime = 0,
  percentageSeen = 0,
  totalClicks = 0,
  adElement = document.getElementById("ad");

/**
 * Logs the viewability values in the console
 *
 * @override
 */
window.log = function () {
  console.log("Ad is viewable: ", adIsViewable
    , "\nViewability time of the ad in sec:", viewabilityTime
    , `\nPercentage visible of the ad in viewport: ${percentageSeen}`
    , `\nTotal clicks: ${totalClicks}`
  );
};

/************************************************************************************************
 *                                                                                              *
 *                              YOUR IMPLEMENTATION                                             *
 *                                                                                              *
 ************************************************************************************************/

var viewableSol = {
  stageHeight: 0,
  interval: 500,
  isFocus: false,
  intervalMeasuring: null,
  limitPercentageSeen: 50,
  init: function() {
    if (!adElement) return false;
    
    // get height
    viewableSol.stageHeight = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    // listeners
    window.addEventListener("focus", function() {
      viewableSol.isFocus = true;
      viewableSol.startMeasuring();
    });
    window.addEventListener("blur", function() {
      viewableSol.isFocus = false;
      viewableSol.stopMeasuring();
    });
    document.getElementById('ad').addEventListener('click', viewableSol.countClicks);

    // start measuring in step 0
    viewableSol.isFocus = true;
    viewableSol.startMeasuring();
  },
  /**
   * This method is un-used in this version.
   * However, in case we would like to check 100% visibility, we should use this method
   * which is faster I think
   */
  elementIsInView: function () {
    // Get the bounding rectangle of our element
    var elementBoundingBox = adElement.getBoundingClientRect();
    var elementsTopY = elementBoundingBox.top;
    var elementsBottomY = elementBoundingBox.top + elementBoundingBox.height;
    return elementsTopY >= 0 && elementsBottomY < viewableSol.stageHeight;
  },
  showResponse: function() {
    if (viewableSol.isFocus) {
      percentageSeen = viewableSol.percentageSeen();
      if (percentageSeen >= viewableSol.limitPercentageSeen) {
        adIsViewable = true;
        viewabilityTime += viewableSol.interval / 1000;
      } else {
        adIsViewable = false;
      }
      window.log()
    }
  },
  startMeasuring: function() {
    if (!viewableSol.intervalMeasuring) {
      viewableSol.intervalMeasuring = setInterval(viewableSol.showResponse, viewableSol.interval);
    }
  },
  stopMeasuring: function() {
    clearInterval(viewableSol.intervalMeasuring);
    viewableSol.intervalMeasuring = null;
  },
  percentageSeen: function () {
    const viewportHeight = window.innerHeight;
    const viewportTop = window.scrollY;
    var viewportBottom = (viewportTop + viewportHeight);
    var elementBoundingBox = adElement.getBoundingClientRect();
    var elementTop = elementBoundingBox.x;
    var elementHeight = elementBoundingBox.height;
    var elementBottom = elementTop + elementHeight;
    var totalHeight = Math.max(elementBottom, viewportBottom) - Math.min(elementTop, viewportTop);
    var heightDiff = totalHeight - viewportHeight;
    var elementInside = elementHeight - heightDiff;
    return parseInt(elementInside <= 0 ? 0 : elementInside / elementHeight * 100);
  },
  countClicks: function () {
    totalClicks += 1;
  },
};

document.onreadystatechange = function () {
  if (document.readyState === 'complete') {
    viewableSol.init();
  }
}

