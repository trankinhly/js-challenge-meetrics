# Requirements
Please read requirements in [link](requirements.md).

# Workflow
* Run `index.html` and open the develop tool to see logs console.
* You may see 4 lines with pattern:
```
Ad is viewable:  false 
Viewability time of the ad in sec: 0 
Percentage visible of the ad in viewport: 0 
Total clicks: 0
```
* Limited of `viewable` ads is `50%` which can change easily by changing value of `limitPercentageSeen` variable in `script.js` file.

# Tech stack
* Structure project contains: 
  * index.html
    * Displaying page content
  * script.js
    * There is `viewableSol` object which is main class to handle cases in the coding challenge.
    * Some main methods:
      * `init`: initializing after page is loaded. We listen events [`focus`, `blur`] of `window` and [`click`] of ads
      * `startMeasuring`: start measuring with being interval `500` second
      * `stopMeasuring`: stop measuring
      * `percentageSeen`: calculating percentage visible of ads when comparing with viewport
      * `countClicks`: handling `click` event of ads
      * `showResponse`: calculating information and call `window.log` to show response
* Supported browsers: Firefox, Chrome, Safari

# Next phase
* Support other browsers: Internet Explorer, Microsoft Edge
* Improve `viewableSol` object to handle multiple ads in one page
* Show response in better UI instead of log console
